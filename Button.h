///////////////////////////////////////////////////////////
//  Button.h
//  Implementation of the Interface Button
//  Created on:      2018/10/23 11:27:20
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_17BB4615_A9DB_49f0_AA83_48652F0A44B4__INCLUDED_)
#define EA_17BB4615_A9DB_49f0_AA83_48652F0A44B4__INCLUDED_

class Button
{

public:
	Button() {

	}

	virtual ~Button() {

	}

	virtual int SenceKey(int key) =0;

};
#endif // !defined(EA_17BB4615_A9DB_49f0_AA83_48652F0A44B4__INCLUDED_)
