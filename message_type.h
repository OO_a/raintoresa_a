/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: message_type
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Common\message_type\message_type.h
*********************************************************************/

#ifndef message_type_H
#define message_type_H

//## package Mechanism::Common::message_type


//## type MESSAGE_TYPE
typedef unsigned long MESSAGE_TYPE;

//#[ type MSG_TIMEOUT
#define MSG_TIMEOUT	0
//#]
#define MSG_STEERING_TASK_START 1

#define MSG_STEERING_TASK_STOP 2

#define MSG_RUN_MODE 3

#define MSG_SELECT_INPUT 4

#define MSG_RUN_INPUT 5

#endif
/*********************************************************************
	File Path	: Mechanism\Common\message_type\message_type.h
*********************************************************************/
