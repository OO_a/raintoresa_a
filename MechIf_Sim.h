/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: MechIf_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\MechIf_Sim.h
*********************************************************************/

#ifndef MechIf_Sim_H
#define MechIf_Sim_H

//## class MechIf_Sim
#include "MechIf.h"
//## package Mechanism::Sim

//## class MechIf_Sim
class MechIf_Sim : public MechIf {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    MechIf_Sim();
    
    //## operation ~MechIf_Sim()
    virtual ~MechIf_Sim();
    
    ////    Operations    ////
    
    //## operation init_cpp_arch()
    VOID init_cpp_arch();
    
    //## operation create_task(Task*,SSHORT,SCHAR* [])
    SSHORT create_task(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]);
    
    //## operation delete_task(SSHORT)
    VOID delete_task(SSHORT rv_tid);
    
    //## operation kill_task(SSHORT)
    VOID kill_task(SSHORT rv_tid);
    
    //## operation send_message(MESSAGE_TYPE,VOID *,MessageReceiver*)
    VOID send_message(MESSAGE_TYPE rv_type, VOID * rv_arg, MessageReceiver* rv_dest);
    
    //## operation set_timer(SSHORT,MessageReceiver*)
    ULONG set_timer(SSHORT rv_time, MessageReceiver* rv_receiver);
    
    //## operation cancel_timer(ULONG)
    BOOL cancel_timer(ULONG rv_id);
    
    //## operation wait(SSHORT)
    VOID wait(SSHORT msec);
};

#endif
/*********************************************************************
	File Path	: Mechanism\Sim\MechIf_Sim.h
*********************************************************************/
