/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Task_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\Task_Sim.h
*********************************************************************/

#ifndef Task_Sim_H
#define Task_Sim_H

//## class Task_Sim
#include "Task.h"
//## auto_generated
class List;

//## dependency ListIterator
class ListIterator;

//## dependency Message
class Message;

//## dependency TimerMessage
class TimerMessage;

//## package Mechanism::Sim

//## class Task_Sim
class Task_Sim : public Task {
    ////    Constructors and destructors    ////
    
public :

    //## operation Task_Sim(SSHORT,sem_t*,priority_t,size_t)
    Task_Sim(SSHORT rv_id, sem_t* rv_sem, priority_t rv_pri, size_t rv_size = DEFAULT_STACK_SIZE);
    
    //## operation ~Task_Sim()
    virtual ~Task_Sim();
    
    ////    Operations    ////

private :

    //## operation receive_message(SSHORT)
    VOID receive_message(SSHORT rv_tid);
    
    //## operation start(SSHORT,SCHAR* [])
    VOID start(SSHORT rv_argc, SCHAR* rv_argv[]);
};

#endif
/*********************************************************************
	File Path	: Mechanism\Sim\Task_Sim.h
*********************************************************************/
