/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: cpparch
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\cpparch\cpparch.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "cpparch.h"
//## dependency Message
#include "Message.h"
//## dependency MessageReceiver
#include "MessageReceiver.h"
//## dependency Task
#include "Task.h"
//## dependency TimerMessage
#include "TimerMessage.h"
//## dependency TimerTask_Real
#include "TimerTask_Real.h"
//## auto_generated
#include <stdlib.h>
//## auto_generated
#include "use.h"
//## auto_generated
#include "slos.h"
//## package Mechanism::Real::cpparch


//## attribute task_list
static Task* task_list[MAX_TASK_NUMBER];

//## attribute argc_task_a
static SSHORT argc_task_a;

//## attribute argc_task_b
static SSHORT argc_task_b;

//## attribute argv_task_a
static SCHAR** argv_task_a;

//## attribute argv_task_b
static SCHAR** argv_task_b;

//## operation init_task_list()
VOID init_task_list() {
    //#[ operation init_task_list()
    for (SSHORT at_i = 0; at_i < MAX_TASK_NUMBER; at_i++) {
    	task_list[at_i] = (Task*)INVALID_TASK;
    }
    
    //#]
}

//## operation create_task_c(Task*,SSHORT,SCHAR* [] )
SSHORT create_task_c(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation create_task_c(Task*,SSHORT,SCHAR* [] )
    if (task_list[rv_task->getTask_id()] == (Task*)INVALID_TASK) {
    	task_list[rv_task->getTask_id()] = rv_task;
    	
    	SCHAR** at_table = (SCHAR**) calloc(rv_argc, sizeof(SCHAR*));
    
    	for (SSHORT at_i = 0; at_i < rv_argc; at_i++) {
    		at_table[at_i] = rv_argv[at_i];
    	}
    	
    	if ((rv_task->getTask_id()) == TASK_ID_TIMER) {
    		//タイマタスクの起動.
    		wup_tsk((UB)1u);
    		free(at_table);
    	} else if ((rv_task->getTask_id()) == TASK_ID_A) {
    		argc_task_a = rv_argc;
    		argv_task_a = at_table;
    		//タスクA(例では、トレーサタスク)の起動.
    		wup_tsk((UB)2u);
    	} else if ((rv_task->getTask_id()) == TASK_ID_B) {
    		argc_task_b = rv_argc;
    		argv_task_b = at_table;
    		//タスクB(例では、UIタスク)の起動.
    		wup_tsk((UB)3u);
    	} else{
    		free(at_table);
    	}
    
    	return rv_task->getTask_id();
    
    } else {
    	return INVALID_TASK;
    }
    
    //#]
}

//## operation delete_task_c(SSHORT)
VOID delete_task_c(SSHORT rv_id) {
    //#[ operation delete_task_c(SSHORT)
    //タスクは最初から生成されているため、使用しない.
    
    //#]
}

//## operation kill_task_c(SSHORT)
VOID kill_task_c(SSHORT rv_id) {
    //#[ operation kill_task_c(SSHORT)
    //タスクは最初から生成されているため、使用しない.
    
    //#]
}

//## operation start_task(Task*,SSHORT,SCHAR* [] )
SSHORT start_task(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation start_task(Task*,SSHORT,SCHAR* [] )
    rv_task->main(rv_argc, rv_argv);
    
    return 0;
    
    //#]
}

//## operation send_message_c(MESSAGE_TYPE,VOID*,MessageReceiver*)
VOID send_message_c(MESSAGE_TYPE rv_type, VOID* rv_arg, MessageReceiver* rv_dest) {
    //#[ operation send_message_c(MESSAGE_TYPE,VOID*,MessageReceiver*)
    Message* at_message = new Message(rv_type, rv_arg, rv_dest);
    Task* at_task = task_list[rv_dest->getTask_id()];
    at_task->appendMessage(at_message);
    
    //#]
}

//## operation recv_message(SSHORT)
VOID recv_message(SSHORT rv_tid) {
    //#[ operation recv_message(SSHORT)
    Task* at_task = task_list[rv_tid];
    Message* at_message = at_task->removeMessage();
    while (at_message != NULL){
    	if (at_message->getType() == MSG_TIMEOUT) {
    		(at_message->getThe_target())->timeout((ULONG)at_message->getArgument());
    	} else {
    		(at_message->getThe_target())->receive(at_message->getType(), at_message->getArgument());
    	}
    	delete at_message;
    	at_message = at_task->removeMessage();
    }
    
    //#]
}

//## operation set_timer_c(SSHORT,MessageReceiver*)
ULONG set_timer_c(SSHORT rv_time, MessageReceiver* rv_receiver) {
    //#[ operation set_timer_c(SSHORT,MessageReceiver*)
    Message* at_message = new TimerMessage(rv_time, rv_receiver);
    Task* at_task = task_list[TASK_ID_TIMER];
    at_task->appendMessage(at_message);
    
    return (ULONG)at_message;
    
    //#]
}

//## operation cancel_timer_c(ULONG)
BOOL cancel_timer_c(ULONG rv_id) {
    //#[ operation cancel_timer_c(ULONG)
    return ((TimerTask*)(task_list[TASK_ID_TIMER]))->cancel((TimerMessage*)rv_id);
    	
    //#]
}

//## operation init_cpp_arch_c()
VOID init_cpp_arch_c() {
    //#[ operation init_cpp_arch_c()
    /*
     * Initialize task list and semaphores.
     */
    init_task_list();
    
    /*
     * Generate timer task.
     */
    TimerTask_Real* at_timer = new TimerTask_Real(TASK_ID_TIMER, DEFAULT_TIMER_INTERVAL);
    create_task_c(at_timer, 1, (SCHAR**)NULL);
    
    //#]
}

//## operation start_timer_task()
VOID start_timer_task() {
    //#[ operation start_timer_task()
    start_task(task_list[TASK_ID_TIMER], 0, NULL);
    
    //#]
}

//## operation start_a_task()
VOID start_a_task() {
    //#[ operation start_a_task()
    start_task(task_list[TASK_ID_A], argc_task_a, argv_task_a);
    
    //#]
}

//## operation start_b_task()
VOID start_b_task() {
    //#[ operation start_b_task()
    start_task(task_list[TASK_ID_B], argc_task_b, argv_task_b);
    //#]
}

//## operation wait_c(SSHORT)
VOID wait_c(SSHORT rv_msec) {
    //#[ operation wait_c(SSHORT)
    volatile SLONG at_i;
    for( at_i = 0; at_i < 1000 * ( long )rv_msec; at_i++ ){
    }
    
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Real\cpparch\cpparch.cpp
*********************************************************************/
