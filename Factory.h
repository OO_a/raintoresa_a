///////////////////////////////////////////////////////////
//  Factory.h
//  Implementation of the Class Factory
//  Created on:      2018/10/22 16:07:49
//  Original author: p000527170
///////////////////////////////////////////////////////////


#if !defined(EA_6012AF86_F736_400e_8EC5_68D768D99A8E__INCLUDED_)
#define EA_6012AF86_F736_400e_8EC5_68D768D99A8E__INCLUDED_

#include "Button_Real.h"
#include "Input.cpp"
#include "Input.h"
#include "Mode.h"
#include "LED7seg_Real.h"
#include "MechIf.h"
#include "Task.h"
#include "LED7seg_Real.h"
#include "MotorDriver_Real.h"
#include "SensorDriver_Real.h"
#include "Task_Real.h"
#include "TimerTask_Real.h"
#include "MechIf_Real.h"
#include "Task_Sim.h"
#include "TimerTask_Sim.h"
#include "MechIf_Sim.h"


class Factory
{

public:
	Factory();
	~Factory();
	void create(Input * select_in, Input * run_in);

};
#endif // !defined(EA_6012AF86_F736_400e_8EC5_68D768D99A8E__INCLUDED_)
