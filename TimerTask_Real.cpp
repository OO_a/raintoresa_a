/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TimerTask_Real
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\TimerTask_Real.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "TimerTask_Real.h"
//## auto_generated
#include "List.h"
//## dependency ListIterator
#include "ListIterator.h"
//## dependency Message
#include "Message.h"
//## dependency TimerMessage
#include "TimerMessage.h"
//## dependency use
#include "use.h"
//## auto_generated
#include "slos.h"
//## package Mechanism::Real

//## class TimerTask_Real
TimerTask_Real::TimerTask_Real(SSHORT rv_id, SLONG rv_time) : TimerTask(rv_id, rv_time)
  {
    //#[ operation TimerTask_Real(SSHORT,SLONG)
    //#]
}

TimerTask_Real::~TimerTask_Real() {
    //#[ operation ~TimerTask_Real()
    //#]
}

VOID TimerTask_Real::start_timer() {
    //#[ operation start_timer()
    TimerMessage* at_message = (TimerMessage*)NULL;
    ListIterator* at_iter = (ListIterator*)NULL;
    SLONG at_time = 0;
    
    while (1) {
    	at_time = get_tim();
    	tslp_tsk(interval);
    	at_iter = new ListIterator(the_message_list);
    
    	while ((at_message = (TimerMessage*)(at_iter->getItem())) != NULL) {
    		if (at_message->passed(get_tim() - at_time)) {
    			at_iter->next();
    			the_message_list->remove((VOID*)at_message);
    			send_message_c(at_message->getType(), (VOID*)at_message, at_message->getThe_target());
    			delete at_message;
    		} else {
    			at_iter->next();
    		}
    	}
    	delete at_iter;
    }
    
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Real\TimerTask_Real.cpp
*********************************************************************/
