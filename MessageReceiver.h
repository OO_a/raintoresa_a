/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: MessageReceiver
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Common\MessageReceiver.h
*********************************************************************/

#ifndef MessageReceiver_H
#define MessageReceiver_H

//## dependency message_type
#include "message_type.h"
//## package Mechanism::Common

//## class MessageReceiver
class MessageReceiver {
    ////    Constructors and destructors    ////
    
public :

	MessageReceiver();

	//## operation MessageReceiver(SSHORT)
    MessageReceiver(SSHORT rv_id);
    
    //## auto_generated
    virtual ~MessageReceiver();
    
    ////    Operations    ////
    
    //## operation receive(MESSAGE_TYPE,VOID*)
    virtual VOID receive(MESSAGE_TYPE rv_type, VOID* rv_arg);
    
    //## operation timeout(ULONG)
    virtual VOID timeout(ULONG rv_id);
    
    ////    Additional operations    ////
    
    //## auto_generated
    SSHORT getTask_id() const;
    
    ////    Attributes    ////

private :

    SSHORT task_id;		//## attribute task_id
};

#endif
/*********************************************************************
	File Path	: Mechanism\Common\MessageReceiver.h
*********************************************************************/
