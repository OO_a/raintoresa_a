///////////////////////////////////////////////////////////
//  LED7seg.h
//  Implementation of the Interface LED7seg
//  Created on:      2018/10/23 11:27:12
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_FAC7EE4D_F37E_481a_A9FB_41A0C8C53286__INCLUDED_)
#define EA_FAC7EE4D_F37E_481a_A9FB_41A0C8C53286__INCLUDED_

class LED7seg
{

public:
	LED7seg() {

	}

	virtual ~LED7seg() {

	}

	virtual void SetSegment(unsigned char val) =0;

};
#endif // !defined(EA_FAC7EE4D_F37E_481a_A9FB_41A0C8C53286__INCLUDED_)
