///////////////////////////////////////////////////////////
//  LineJudgement.h
//  Implementation of the Class LineJudgement
//  Created on:      2018/10/22 16:09:29
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_E717B447_75B2_4721_9CA4_E7D7AFD3A400__INCLUDED_)
#define EA_E717B447_75B2_4721_9CA4_E7D7AFD3A400__INCLUDED_

#include "LineInfo.h"

class LineJudgement
{

public:
	LineJudgement();
	~LineJudgement();
	bool judgeLine();
	bool judgeCourseOut();
	int init(LineInfo * line_info);

private:
	bool line_state;
	int ref_value;
	LineInfo *the_provider;

};
#endif // !defined(EA_E717B447_75B2_4721_9CA4_E7D7AFD3A400__INCLUDED_)
