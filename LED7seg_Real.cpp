///////////////////////////////////////////////////////////
//  LED7seg_Real.cpp
//  Implementation of the Class LED7seg_Real
//  Created on:      2018/10/23 10:13:15
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "LED7seg_Real.h"
#include "36109f.h"
#include "machine.h"
#include "driver.h"


static unsigned char save_7seg;
static const unsigned char led_table[16] = { 0x03, 0x9f, 0x25, 0x0d, 0x99, 0x49, 0x41, 0x1b,
0x01, 0x09, 0x11, 0xc1, 0x63, 0x85, 0x61, 0x71 };

LED7seg_Real::LED7seg_Real(){

}


LED7seg_Real::~LED7seg_Real(){

}


void LED7seg_Real::SetSegment(unsigned char val){

	unsigned char at_val = val;

	if (at_val > 15) {
		at_val = 15;
	}
	save_7seg = at_val;

	*PDRD = led_table[at_val];
}