///////////////////////////////////////////////////////////
//  Input.cpp
//  Implementation of the Class Input
//  Created on:      2018/10/23 11:59:39
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Input.h"




Input::Input(){
	input_type = 0;
	last_input = 0;
	the_current_mode = 0;
	the_if = 0;
	the_button = 0;
}

Input::Input(int key) {
	input_type = key;
}


Input::~Input(){

}


void Input::action(){
	
	int buttonVal;
	if (input_type == 0) {
		if (the_button->SenceKey(input_type) < last_input) {
			the_if->send_message(MSG_SELECT_INPUT, 0, the_current_mode);
		}
	}
	else if(input_type == 1){
		if (the_button->SenceKey(input_type) < last_input) {
			the_if->send_message(MSG_RUN_INPUT, 0, the_current_mode);
		}
	}
	last_input = the_button->SenceKey(input_type);

}


int Input::init(Mode * mode, MechIf * mechIf,Button * button){
	the_current_mode = mode;
	the_if = mechIf;
	the_button = button;
}