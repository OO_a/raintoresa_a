///////////////////////////////////////////////////////////
//  Factory.cpp
//  Implementation of the Class Factory
//  Created on:      2018/10/22 16:07:49
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Factory.h"



Factory::Factory(){

}


Factory::~Factory(){

}


void Factory::create(Input * select_in, Input * run_in){
	//タスクID格納用
	int task_1;
	int task_2; 
	extern sem_t sem_a;
	extern sem_t sem_b;

	//メカニズム用インスタンス生成初期化

	//シミュレーション用
	MechIf *mech_if = new MechIf_Sim;
	mech_if->init_cpp_arch();
	Task *task1 = new Task_Sim(TASK_ID_A, &sem_a,PRIO_NORMAL);
	task_1 = mech_if->create_task(task1, 1, NULL);
	Task *task2 = new Task_Sim(TASK_ID_B, &sem_b, PRIO_NORMAL);;
	task_2 = mech_if->create_task(task2, 1, NULL);

	//実機用
	//MechIf *mech_if = new MechIf_Real;
	//mech_if->init_cpp_arch();
	//Task *task1 = new Task_Real(TASK_ID_A, &sem_a, PRIO_NORMAL);
	//task_1 = mech_if->create_task(task1, 1, NULL);
	//Task *task2 = new Task_Real(TASK_ID_B, &sem_b, PRIO_NORMAL);;
	//task_2 = mech_if->create_task(task2, 1, NULL);

	//実装インスタンス生成
	Button *button = new Button_Real;
	Input *select_button = new Input(0);
	Input *run_button = new Input(1);
	Mode *mode = new Mode(task_1);
	Display *display = new Display;
	LED7seg *led_7seg = new LED7seg_Real;
	Driving *driving = new Driving;
	MotorDriver *motor_driver = new MotorDriver_Real;
	Steering *steering = new Steering(task_2);
	LineJudgement *line_judgement = new LineJudgement;
	LineInfo *line_info = new LineInfo;
	SensorDriver *sensor_driver = new SensorDriver_Real;

	//mainへの受け渡し
	select_in = select_button;
	run_in = run_button;

	//実装インスタンス初期化
	select_button->init(mode, mech_if, button);
	run_button->init(mode, mech_if,button);
	mode->init(mech_if,steering, driving,display);
	display->init(led_7seg);
	driving->init(motor_driver);
	steering->init(line_judgement,mech_if);
	line_judgement->init(line_info);
	line_info->init(sensor_driver);
}