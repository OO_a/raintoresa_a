///////////////////////////////////////////////////////////
//  Steering.h
//  Implementation of the Class Steering
//  Created on:      2018/10/23 11:59:03
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_5A8A4680_15CC_4474_B362_37F7C75DF4A5__INCLUDED_)
#define EA_5A8A4680_15CC_4474_B362_37F7C75DF4A5__INCLUDED_

#include "LineJudgement.h"
#include "MessageReceiver.h"
#include "MechIf.h"
#include "MotorDriver.h"
#include "Kata.h"
#include "driver.h"

class Steering : public MessageReceiver
{

public:
	Steering();
	Steering(int task_id);
	~Steering();
	void controlSteering(int mode_state);
	int init(LineJudgement * line_judgment, MechIf * mech_if);
	int timeout(unsigned long id);
	void receive(MESSAGE_TYPE type, void * arg);

private:
	int steering_range;
	unsigned long timer_id;
	int right_max;
	int left_max;
	LineJudgement *the_direction;
	MechIf *the_if;
	MotorDriver *the_steer_motor;
	bool result_timer;
};
#endif // !defined(EA_5A8A4680_15CC_4474_B362_37F7C75DF4A5__INCLUDED_)
