/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: cpparch
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\cpparch\cpparch.h
*********************************************************************/

#ifndef cpparch_H
#define cpparch_H

//## dependency message_type
#include "message_type.h"
//## dependency Message
class Message;

//## dependency MessageReceiver
class MessageReceiver;

//## dependency Task
class Task;

//## dependency TimerMessage
class TimerMessage;

//## dependency TimerTask_Real
class TimerTask_Real;

//## package Mechanism::Real::cpparch


//#[ type MAX_TASK_NUMBER
#define MAX_TASK_NUMBER	10
//#]

//#[ type INVALID_TASK
#define INVALID_TASK	-1
//#]

//#[ type DEFAULT_TIMER_INTERVAL
#define DEFAULT_TIMER_INTERVAL	1
//#]

//## operation init_task_list()
VOID init_task_list();

//## operation create_task_c(Task*,SSHORT,SCHAR* [] )
SSHORT create_task_c(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]);

//## operation delete_task_c(SSHORT)
VOID delete_task_c(SSHORT rv_id);

//## operation kill_task_c(SSHORT)
VOID kill_task_c(SSHORT rv_id);

//## operation start_task(Task*,SSHORT,SCHAR* [] )
SSHORT start_task(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]);

//## operation send_message_c(MESSAGE_TYPE,VOID*,MessageReceiver*)
VOID send_message_c(MESSAGE_TYPE rv_type, VOID* rv_arg, MessageReceiver* rv_dest);

//## operation recv_message(SSHORT)
VOID recv_message(SSHORT rv_tid);

//## operation set_timer_c(SSHORT,MessageReceiver*)
ULONG set_timer_c(SSHORT rv_time, MessageReceiver* rv_receiver);

//## operation cancel_timer_c(ULONG)
BOOL cancel_timer_c(ULONG rv_id);

//## operation init_cpp_arch_c()
VOID init_cpp_arch_c();

//## operation start_timer_task()

//#[ ignore
#ifdef __cplusplus
extern "C" {
#endif
//#]
VOID start_timer_task();
//#[ ignore
#ifdef __cplusplus
}
#endif
//#]


//## operation start_a_task()

//#[ ignore
#ifdef __cplusplus
extern "C" {
#endif
//#]
VOID start_a_task();
//#[ ignore
#ifdef __cplusplus
}
#endif
//#]


//## operation start_b_task()

//#[ ignore
#ifdef __cplusplus
extern "C" {
#endif
//#]
VOID start_b_task();
//#[ ignore
#ifdef __cplusplus
}
#endif
//#]


//## operation wait_c(SSHORT)
VOID wait_c(SSHORT rv_msec);

#endif
/*********************************************************************
	File Path	: Mechanism\Real\cpparch\cpparch.h
*********************************************************************/
