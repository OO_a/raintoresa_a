///////////////////////////////////////////////////////////
//  Input.h
//  Implementation of the Class Input
//  Created on:      2018/10/23 11:59:39
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_C6B04D46_F065_45ee_80FA_C37EB576D963__INCLUDED_)
#define EA_C6B04D46_F065_45ee_80FA_C37EB576D963__INCLUDED_

#include "Mode.h"
#include "MechIf.h"
#include "Button.h"

class Input
{

public:
	Input();
	Input(int key);
	~Input();
	void action();
	int init(Mode * mode, MechIf * mechIf,Button * button);

private:
	int input_type;
	int last_input;
	Mode *the_current_mode;
	MechIf *the_if;
	Button *the_button;

};
#endif // !defined(EA_C6B04D46_F065_45ee_80FA_C37EB576D963__INCLUDED_)
