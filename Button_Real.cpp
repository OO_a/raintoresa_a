///////////////////////////////////////////////////////////
//  Button_Real.cpp
//  Implementation of the Class Button_Real
//  Created on:      2018/10/22 16:11:45
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Button_Real.h"
#include "36109f.h"
#include "machine.h"
#include "driver.h"




Button_Real::Button_Real(){

}


Button_Real::~Button_Real(){

}

/*
*  プッシュスイッチドライバ
*/
int Button_Real::SenceKey(int key){
	int result = DEV_KEYOFF;
	unsigned char sw;

	if (key < NUM_KEY) {
		sw = *PDR1;
		if (((0x80 >> key) & sw) == 0)
			result = DEV_KEYON;
	}
	return result;
}