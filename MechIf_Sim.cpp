/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: MechIf_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\MechIf_Sim.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "MechIf_Sim.h"
//## package Mechanism::Sim

//## class MechIf_Sim
MechIf_Sim::MechIf_Sim() {
}

MechIf_Sim::~MechIf_Sim() {
    //#[ operation ~MechIf_Sim()
    //#]
}

VOID MechIf_Sim::init_cpp_arch() {
    //#[ operation init_cpp_arch()
    //#]
}

SSHORT MechIf_Sim::create_task(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation create_task(Task*,SSHORT,SCHAR* [])
    return rv_task->main(rv_argc, rv_argv);
    
    //#]
}

VOID MechIf_Sim::delete_task(SSHORT rv_tid) {
    //#[ operation delete_task(SSHORT)
    //#]
}

VOID MechIf_Sim::kill_task(SSHORT rv_tid) {
    //#[ operation kill_task(SSHORT)
    //#]
}

VOID MechIf_Sim::send_message(MESSAGE_TYPE rv_type, VOID * rv_arg, MessageReceiver* rv_dest) {
    //#[ operation send_message(MESSAGE_TYPE,VOID *,MessageReceiver*)
    //#]
}

ULONG MechIf_Sim::set_timer(SSHORT rv_time, MessageReceiver* rv_receiver) {
    //#[ operation set_timer(SSHORT,MessageReceiver*)
    return 0;
    
    //#]
}

BOOL MechIf_Sim::cancel_timer(ULONG rv_id) {
    //#[ operation cancel_timer(ULONG)
      return TRUE;
    //#]
}

VOID MechIf_Sim::wait(SSHORT msec) {
    //#[ operation wait(SSHORT)
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Sim\MechIf_Sim.cpp
*********************************************************************/
