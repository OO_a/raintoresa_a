/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: MechIf_Real
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\MechIf_Real.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "MechIf_Real.h"
//## package Mechanism::Real

//## class MechIf_Real
MechIf_Real::MechIf_Real() {
}

MechIf_Real::~MechIf_Real() {
    //#[ operation ~MechIf_Real()
    //#]
}

VOID MechIf_Real::init_cpp_arch() {
    //#[ operation init_cpp_arch()
    init_cpp_arch_c();
    
    //#]
}

SSHORT MechIf_Real::create_task(Task* rv_task, SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation create_task(Task*,SSHORT,SCHAR* [])
    return (create_task_c(rv_task, rv_argc, rv_argv));
    
    //#]
}

VOID MechIf_Real::delete_task(SSHORT rv_tid) {
    //#[ operation delete_task(SSHORT)
    delete_task_c(rv_tid);
    
    //#]
}

VOID MechIf_Real::kill_task(SSHORT rv_tid) {
    //#[ operation kill_task(SSHORT)
    kill_task_c(rv_tid);
    
    //#]
}

VOID MechIf_Real::send_message(MESSAGE_TYPE rv_type, VOID* rv_arg, MessageReceiver* rv_dest) {
    //#[ operation send_message(MESSAGE_TYPE,VOID*,MessageReceiver*)
    send_message_c(rv_type, rv_arg, rv_dest);
    
    //#]
}

ULONG MechIf_Real::set_timer(SSHORT rv_time, MessageReceiver* rv_receiver) {
    //#[ operation set_timer(SSHORT,MessageReceiver*)
    return (set_timer_c(rv_time, rv_receiver));
    
    //#]
}

BOOL MechIf_Real::cancel_timer(ULONG rv_id) {
    //#[ operation cancel_timer(ULONG)
    return (cancel_timer_c(rv_id));
    
    //#]
}

VOID MechIf_Real::wait(SSHORT rv_msec) {
    //#[ operation wait(SSHORT)
    wait_c(rv_msec);
    
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Real\MechIf_Real.cpp
*********************************************************************/
