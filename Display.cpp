///////////////////////////////////////////////////////////
//  Display.cpp
//  Implementation of the Class Display
//  Created on:      2018/10/23 11:59:30
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Display.h"
#include "Mode.h"



Display::Display(){
	display_method = 0;
	the_led = 0;
}


Display::~Display(){

}


void Display::show(Mode_state mode_state){
	switch (mode_state) {
		case eSTOP:
			display_method = 0;
			break;
		case eRUN_WAIT:
			display_method = 1;
			break;
		case eRUN:
			display_method = 1;
			break;
		case eSTOP_WAIT:
			display_method = 0;
			break;
		default:
			break;
	}
	the_led->SetSegment(display_method);
}


int Display::init(LED7seg* led_7seg){
	the_led = led_7seg;
}