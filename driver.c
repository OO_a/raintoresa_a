

#include "36109f.h"
#include "machine.h"
#include "driver.h"

#ifdef __cplusplus
extern "C" {
#endif


static unsigned char save_7seg;
static const unsigned char led_table[16] = {0x03, 0x9f, 0x25, 0x0d, 0x99, 0x49, 0x41, 0x1b,
											0x01, 0x09, 0x11, 0xc1, 0x63, 0x85, 0x61, 0x71};

/*
 *  7SEGドライバ設定
 */
void SetSegment(unsigned char val)
{
	unsigned char at_val = val;
	
	if(at_val > 15){
		at_val = 15;
	}
	save_7seg = at_val;
	
	*PDRD = led_table[at_val];
}

/*
 *  7SEGドライバ取得
 */
unsigned char GetSegment(void)
{
	return save_7seg;
}

/*
 *  プッシュスイッチドライバ
 */
int SenceKey(int key)
{
	int result = DEV_KEYOFF;
	unsigned char sw;

	if(key < NUM_KEY){
		sw = *PDR1;
		if(((0x80 >> key) & sw) == 0)
			result = DEV_KEYON;
	}
	return result;
}

/*
 *  反射型センサー
 */
unsigned short GetSensor(void)
{
	return *ADDRA;
}

/*
 *  駆動モータ設定
 */
static void SetMotorA(int action, unsigned short speed)
{
	unsigned char reg = *PDR3;
	speed &= 0x3fff;
	switch(action){
	case MOTOR_FORE:
		*PWDRL = (unsigned char)(speed & 0xff);
		*PWDRU = (unsigned char)(speed >> 8);
		*PDR3 = reg | 0x80;
		break;
	case MOTOR_BACK:
		speed = ~speed;
		*PWDRL = (unsigned char)(speed & 0xff);
		*PWDRU = (unsigned char)(speed >> 8);
		*PDR3 = reg & ~0x80;
		break;
	default:
		*PWDRL = 0xff;
		*PWDRU = 0xff;
		*PDR3 = reg | 0x80;
		break;
	}
} 

/*
 *  ハンドルモータ設定
 */
static void SetMotorB(int action)
{
	unsigned char reg = *PDR3;
	switch(action){
	case MOTOR_FORE:
		*PDR3 = (reg & ~0x60) | 0x40;
		break;
	case MOTOR_BACK:
		*PDR3 = (reg & ~0x60) | 0x20;
		break;
	default:
		*PDR3 = reg | 0x60;
		break;
	}
}

/*
 *  モータ設定
 */
void SetMotor(int port, int action, unsigned short speed)
{
	if(port > NUM_MOTOR){
		return;
	}
	
	switch(port){
	case eMotorA:
		SetMotorA(action, speed);
		break; 
	case eMotorB:
		SetMotorB(action);
		break;
	default:
		break;
	}
}

#ifdef __cplusplus
}
#endif

