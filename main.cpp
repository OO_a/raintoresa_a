///////////////////////////////////////////////////////////
//  main.cpp
///////////////////////////////////////////////////////////

#include "main.h"

main() {
	Input *select_in;
	Input *run_in;
	Factory *factory = new Factory;
	factory->create(select_in, run_in);
	while (1) {
		select_in->action();
		std::this_thread::sleep_for(std::chrono::microseconds(1));
		run_in->action();
		std::this_thread::sleep_for(std::chrono::microseconds(1));
	}

}