/*******************************************/
/*						*/
/*		H8/36109 ADDRESS		*/
/*						*/
/************************************************/

#ifndef _36109F_H_
#define _36109F_H_

#define	SMR_3		(volatile unsigned char*)(0xFFF600)
#define	BRR_3		(volatile unsigned char*)(0xFFF601)
#define	SCR3_3		(volatile unsigned char*)(0xFFF602)
#define	TDR_3		(volatile unsigned char*)(0xFFF603)
#define	SSR_3		(volatile unsigned char*)(0xFFF604)
#define	RDR_3		(volatile unsigned char*)(0xFFF605)
#define	SMCR		(volatile unsigned char*)(0xFFF608)
#define	ADDRA		(volatile unsigned short*)(0xFFF610)

#define	SMR		(volatile unsigned char*)(0xFFFFA8)
#define	BRR		(volatile unsigned char*)(0xFFFFA9)
#define	SCR3		(volatile unsigned char*)(0xFFFFAA)
#define	TDR		(volatile unsigned char*)(0xFFFFAB)
#define	SSR		(volatile unsigned char*)(0xFFFFAC)
#define	RDR		(volatile unsigned char*)(0xFFFFAD)

#define TCSRWD 		(volatile unsigned char*)(0xFFFFC0)
#define TCWD 		(volatile unsigned char*)(0xFFFFC1)
#define TMWD 		(volatile unsigned char*)(0xFFFFC2)

#define PMR1 		(volatile unsigned char*)(0xFFFFE0)
#define	RCCR		(volatile unsigned char*)(0xFFF738)
#define	RCTRMDPR	(volatile unsigned char*)(0xFFF739)
#define	RCTRMDR		(volatile unsigned char*)(0xFFF73A)
#define	CKCSR		(volatile unsigned char*)(0xFFF734)

#define	TMB1		(volatile unsigned char*)(0xFFF760)
#define	TCB1		(volatile unsigned char*)(0xFFF761)
#define	TLB1		(volatile unsigned char*)(0xFFF762)

#define ADCSR		(volatile unsigned char*)(0xFFF618)
#define ADCR		(volatile unsigned char*)(0xFFF619)
#define PDRD		(volatile unsigned char*)(0xFFF624)
#define PDRF		(volatile unsigned char*)(0xFFF626)
#define PMRF		(volatile unsigned char*)(0xFFF630)
#define PCRD		(volatile unsigned char*)(0xFFF634)
#define	PWDRL		(volatile unsigned char*)(0xFFFFBC)
#define	PWDRU		(volatile unsigned char*)(0xFFFFBD)
#define	PWCR		(volatile unsigned char*)(0xFFFFBE)
#define PUCR1		(volatile unsigned char*)(0xFFFFD0)
#define PDR1		(volatile unsigned char*)(0xFFFFD4)
#define PDR3		(volatile unsigned char*)(0xFFFFD6)
#define PMR1		(volatile unsigned char*)(0xFFFFE0)
#define PMR3		(volatile unsigned char*)(0xFFFFE2)
#define PCR1		(volatile unsigned char*)(0xFFFFE4)
#define PCR3		(volatile unsigned char*)(0xFFFFE6)

#define	IENR2		(volatile unsigned char*)(0xFFFFF5)
#define	IRR2		(volatile unsigned char*)(0xFFFFF7)

#endif	/* _36109F_H_ */

