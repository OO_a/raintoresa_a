///////////////////////////////////////////////////////////
//  Mode.h
//  Implementation of the Class Mode
//  Created on:      2018/10/22 16:08:46
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_7BE29960_44F7_4f6e_B186_01CE77F7852B__INCLUDED_)
#define EA_7BE29960_44F7_4f6e_B186_01CE77F7852B__INCLUDED_

#include "Display.h"
#include "Driving.h"
#include "Steering.h"
#include "MechIf.h"
#include "MessageReceiver.h"
#include "Kata.h"

class Mode : public MessageReceiver
{

public:
	Mode();
	Mode(int task_id);
	~Mode();
	void runMode();
	void switchMode();
	int init(MechIf * mech_if, Steering * steering, Driving * driving, Display * display);
	void receive(MESSAGE_TYPE type, void * arg);

private:
	Mode_state mode_state;
	Display *the_display;
	Driving *the_driving;
	Steering *the_steering;
	MechIf *the_if;
};
#endif // !defined(EA_7BE29960_44F7_4f6e_B186_01CE77F7852B__INCLUDED_)