
#ifndef _DRIVER_H_
#define	_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
 *  プッシュスイッチ関係の定義
 */
enum DEVKEY {
	eDevKey1 = 0,
	eDevKey2,
	NUM_KEY
};


#define DEV_KEYON	1			/* KEYオン状態 */
#define	DEV_KEYOFF	0			/* KEYオフ状態 */

/*
 *  モータの定義
 */
enum DEVMOTOR {
	eMotorA = 0,
	eMotorB,
	NUM_MOTOR
};

#define MOTOR_STOP	0			/* 停止 */
#define MOTOR_FORE  1			/* 前進 */
#define MOTOR_BACK  2			/* 後退 */


extern void SetSegment(unsigned char val);
extern unsigned char GetSegment(void);
extern int SenceKey(int key);
extern unsigned short GetSensor(void);
extern void SetMotor(int port, int action, unsigned short speed);



#ifdef __cplusplus
}
#endif

#endif	/* _DRIVER_H_ */

