/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Task_Real
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\Task_Real.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "Task_Real.h"
//## dependency cpparch
#include "cpparch.h"
//## auto_generated
#include "List.h"
//## dependency ListIterator
#include "ListIterator.h"
//## dependency Message
#include "Message.h"
//## dependency TimerMessage
#include "TimerMessage.h"
//## auto_generated
#include "slos.h"
//## package Mechanism::Real

//## class Task_Real
Task_Real::Task_Real(SSHORT rv_id, sem_t* rv_sem, priority_t rv_pri, size_t rv_size) : Task(rv_id, rv_sem, rv_pri, rv_size) {
    //#[ operation Task_Real(SSHORT,sem_t*,priority_t,size_t)
    //#]
}

Task_Real::~Task_Real() {
    //#[ operation ~Task_Real()
    //#]
}

VOID Task_Real::receive_message(SSHORT rv_tid) {
    //#[ operation receive_message(SSHORT)
    while (1) {
    	recv_message(rv_tid);
    	tslp_tsk(1);
    }
    
    //#]
}

VOID Task_Real::start(SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation start(SSHORT,SCHAR* [])
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Real\Task_Real.cpp
*********************************************************************/
