///////////////////////////////////////////////////////////
//  Mode.cpp
//  Implementation of the Class Mode
//  Created on:      2018/10/22 16:08:46
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Mode.h"

using namespace std;

Mode::Mode() {
	mode_state = eSTOP;
	the_display = 0;
	the_driving = 0;
	the_steering = 0;
	the_if = 0;
}

Mode::Mode(int task_id){

}


Mode::~Mode(){

}


void Mode::runMode(){
	switch (mode_state){
	case eSTOP:
		break;
	case eRUN_WAIT:
		mode_state = eRUN;
		the_display->show(mode_state); //表示
		the_driving->controlDriving(mode_state); 
		/*steeringに非同期メッセージ送る*/
		the_if->send_message(MSG_STEERING_TASK_START,&mode_state,the_steering);
		break;
	case eRUN:
		mode_state = eSTOP;
		the_display->show(mode_state);
		the_driving->controlDriving(mode_state);
		/*steeringに非同期メッセージ送る*/
		the_if->send_message(MSG_STEERING_TASK_STOP, &mode_state, the_steering);
		break;
	case eSTOP_WAIT:
		mode_state = eSTOP;
		the_display->show(mode_state);
		the_driving->controlDriving(mode_state);
		/*steeringに非同期メッセージ送る*/
		the_if->send_message(MSG_STEERING_TASK_STOP, &mode_state, the_steering);
		break;
	}
}


void Mode::switchMode(){
	switch (mode_state) {
		case eSTOP:
			mode_state = eRUN_WAIT;
			break;
		case eRUN_WAIT:
			mode_state = eSTOP;
			break;
		case eRUN:
			mode_state = eSTOP_WAIT;
			break;
		case eSTOP_WAIT:
			mode_state = eSTOP;
			break;
	}
	/*表示*/
	the_display->show(mode_state);

}


int Mode::init(MechIf * mech_if, Steering * steering, Driving * driving, Display * display){
	the_display = display;
	the_driving = driving;
	the_steering = steering;
	the_if = mech_if;
	return 0;
}


void Mode::receive(MESSAGE_TYPE type, void * arg){
	switch (type)
	{
	case MSG_SELECT_INPUT:
		switchMode;
		break;
	case MSG_RUN_INPUT:
		runMode;
	default:
		break;
	}
}
