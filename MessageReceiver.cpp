/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: MessageReceiver
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Common\MessageReceiver.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "MessageReceiver.h"
//## package Mechanism::Common


MessageReceiver::MessageReceiver() {
	//#[ operation MessageReceiver(SSHORT)
	//#]
}

//## class MessageReceiver
MessageReceiver::MessageReceiver(SSHORT rv_id) : task_id(rv_id) {
    //#[ operation MessageReceiver(SSHORT)
    //#]
}

MessageReceiver::~MessageReceiver() {
}

VOID MessageReceiver::receive(MESSAGE_TYPE rv_type, VOID* rv_arg) {
    //#[ operation receive(MESSAGE_TYPE,VOID*)
    //#]
}

VOID MessageReceiver::timeout(ULONG rv_id) {
    //#[ operation timeout(ULONG)
    //#]
}

SSHORT MessageReceiver::getTask_id() const {
    return task_id;
}

/*********************************************************************
	File Path	: Mechanism\Common\MessageReceiver.cpp
*********************************************************************/
