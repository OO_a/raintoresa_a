/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TimerTask_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\TimerTask_Sim.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "TimerTask_Sim.h"
//## auto_generated
#include "List.h"
//## dependency ListIterator
#include "ListIterator.h"
//## dependency Message
#include "Message.h"
//## dependency TimerMessage
#include "TimerMessage.h"
//## dependency use
#include "use.h"
//## package Mechanism::Sim

//## class TimerTask_Sim
TimerTask_Sim::TimerTask_Sim(SSHORT rv_id, SLONG rv_time) : TimerTask(rv_id, rv_time)
  {
    //#[ operation TimerTask_Sim(SSHORT,SLONG)
    //#]
}

TimerTask_Sim::~TimerTask_Sim() {
    //#[ operation ~TimerTask_Sim()
    //#]
}

VOID TimerTask_Sim::start_timer() {
    //#[ operation start_timer()
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Sim\TimerTask_Sim.cpp
*********************************************************************/
