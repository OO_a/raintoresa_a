///////////////////////////////////////////////////////////
//  Button_Real.h
//  Implementation of the Class Button_Real
//  Created on:      2018/10/22 16:11:45
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_A79F873D_06DD_4e46_BC47_E914BC8DF91B__INCLUDED_)
#define EA_A79F873D_06DD_4e46_BC47_E914BC8DF91B__INCLUDED_

#include "Button.h"

class Button_Real : public Button
{

public:
	Button_Real();
	~Button_Real();
	int SenceKey(int key);

};
#endif // !defined(EA_A79F873D_06DD_4e46_BC47_E914BC8DF91B__INCLUDED_)
