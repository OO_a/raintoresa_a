///////////////////////////////////////////////////////////
//  SensorDriver_Real.h
//  Implementation of the Class SensorDriver_Real
//  Created on:      2018/10/22 16:11:18
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_398DF89A_78E1_42c0_983A_5676E4CEE7D3__INCLUDED_)
#define EA_398DF89A_78E1_42c0_983A_5676E4CEE7D3__INCLUDED_

#include "SensorDriver.h"

class SensorDriver_Real : public SensorDriver
{

public:
	SensorDriver_Real();
	~SensorDriver_Real();
	unsigned short GetSensor();
};
#endif // !defined(EA_398DF89A_78E1_42c0_983A_5676E4CEE7D3__INCLUDED_)
