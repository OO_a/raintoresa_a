///////////////////////////////////////////////////////////
//  Display.h
//  Implementation of the Class Display
//  Created on:      2018/10/23 11:59:30
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_C731F266_8925_4821_B13D_BABBBC460819__INCLUDED_)
#define EA_C731F266_8925_4821_B13D_BABBBC460819__INCLUDED_

#include "LED7seg.h"
#include "Kata.h"

class Display
{

public:
	Display();
	~Display();
	void show(Mode_state mode_state);
	int init(LED7seg* led_7seg);

private:
	unsigned char display_method;
	LED7seg* the_led;

};
#endif // !defined(EA_C731F266_8925_4821_B13D_BABBBC460819__INCLUDED_)
