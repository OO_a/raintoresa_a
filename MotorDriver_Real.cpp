///////////////////////////////////////////////////////////
//  MotorDriver_Real.cpp
//  Implementation of the Class MotorDriver_Real
//  Created on:      2018/10/22 16:11:29
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "MotorDriver_Real.h"
#include "36109f.h"
#include "machine.h"
#include "driver.h"



MotorDriver_Real::MotorDriver_Real(){

}


MotorDriver_Real::~MotorDriver_Real(){

}


/*
*  駆動モータ設定
*/
void MotorDriver_Real::SetMotorA(int action, unsigned short speed)
{
	unsigned char reg = *PDR3;
	speed &= 0x3fff;
	switch (action) {
	case MOTOR_FORE:
		*PWDRL = (unsigned char)(speed & 0xff);
		*PWDRU = (unsigned char)(speed >> 8);
		*PDR3 = reg | 0x80;
		break;
	case MOTOR_BACK:
		speed = ~speed;
		*PWDRL = (unsigned char)(speed & 0xff);
		*PWDRU = (unsigned char)(speed >> 8);
		*PDR3 = reg & ~0x80;
		break;
	default:
		*PWDRL = 0xff;
		*PWDRU = 0xff;
		*PDR3 = reg | 0x80;
		break;
	}
}

/*
*  操舵モータ設定
*/
void MotorDriver_Real::SetMotorB(int action)
{
	unsigned char reg = *PDR3;
	switch (action) {
	case MOTOR_FORE:
		*PDR3 = (reg & ~0x60) | 0x40;
		break;
	case MOTOR_BACK:
		*PDR3 = (reg & ~0x60) | 0x20;
		break;
	default:
		*PDR3 = reg | 0x60;
		break;
	}
}

/*
*  モータ設定
*/
void MotorDriver_Real::SetMotor(int port, int action, unsigned short speed)
{
	if (port > NUM_MOTOR) {
		return;
	}

	switch (port) {
	case eMotorA:
		SetMotorA(action, speed);	/*駆動モータを動かす*/
		break;
	case eMotorB:
		SetMotorB(action);	/*操舵モータを動かす*/
		break;
	default:
		break;
	}
}