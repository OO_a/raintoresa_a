///////////////////////////////////////////////////////////
//  LED7seg_Real.h
//  Implementation of the Class LED7seg_Real
//  Created on:      2018/10/23 10:13:15
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_2E727221_862B_420a_BD84_E438C2DAF58A__INCLUDED_)
#define EA_2E727221_862B_420a_BD84_E438C2DAF58A__INCLUDED_

#include "LED7seg.h"

class LED7seg_Real : public LED7seg
{

public:
	LED7seg_Real();
	~LED7seg_Real();
	void SetSegment(unsigned char val);

};
#endif // !defined(EA_2E727221_862B_420a_BD84_E438C2DAF58A__INCLUDED_)
