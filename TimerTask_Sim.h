/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TimerTask_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\TimerTask_Sim.h
*********************************************************************/

#ifndef TimerTask_Sim_H
#define TimerTask_Sim_H

//## class TimerTask_Sim
#include "TimerTask.h"
//## auto_generated
class List;

//## dependency ListIterator
class ListIterator;

//## dependency Message
class Message;

//## dependency TimerMessage
class TimerMessage;

//## package Mechanism::Sim

//## class TimerTask_Sim
class TimerTask_Sim : public TimerTask {
    ////    Constructors and destructors    ////
    
public :

    //## operation TimerTask_Sim(SSHORT,SLONG)
    TimerTask_Sim(SSHORT rv_id, SLONG rv_time);
    
    //## operation ~TimerTask_Sim()
    virtual ~TimerTask_Sim();
    
    ////    Operations    ////

private :

    //## operation start_timer()
    VOID start_timer();
};

#endif
/*********************************************************************
	File Path	: Mechanism\Sim\TimerTask_Sim.h
*********************************************************************/
