///////////////////////////////////////////////////////////
//  SensorDriver.h
//  Implementation of the Interface SensorDriver
//  Created on:      2018/10/23 11:15:47
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_7C87AC81_E743_443c_A48E_9215C2184588__INCLUDED_)
#define EA_7C87AC81_E743_443c_A48E_9215C2184588__INCLUDED_

class SensorDriver
{

public:
	SensorDriver() {

	}

	virtual ~SensorDriver() {

	}

	virtual unsigned short GetSensor() =0;

};
#endif // !defined(EA_7C87AC81_E743_443c_A48E_9215C2184588__INCLUDED_)
