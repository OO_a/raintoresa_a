///////////////////////////////////////////////////////////
//  LineJudgement.cpp
//  Implementation of the Class LineJudgement
//  Created on:      2018/10/22 16:09:29
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "LineJudgement.h"




LineJudgement::LineJudgement(){
	line_state = true;	/*初期値はライン上*/
	ref_value = 15000;	/*閾値(以上でライン上)*/
	the_provider = 0;	/*アドレス初期値*/
}


LineJudgement::~LineJudgement(){

}


bool LineJudgement::judgeLine(){
	int sensVal;	/*センサ値入れとく箱*/
	sensVal = the_provider->getLineInfo ;	/*センサ値拾う*/

	/*ここから判定*/
		if (sensVal >= ref_value) {	/*閾値以上でライン上*/
			return true;
		}
		else {	/*閾値未満でライン外*/
			return false;
		}
}


bool LineJudgement::judgeCourseOut(){

	return false;
}


int LineJudgement::init(LineInfo * line_info){
	the_provider = line_info;	/*ライン情報取得先のアドレス*/
	return 0;
}