/***********************************************************************/
/*                                                                     */
/*  FILE        :sbrk.c                                                */
/*  DESCRIPTION :Program of sbrk                                       */
/*                                                                     */
/*  This file is generated by Hitachi Project Generator (Ver.2.4).     */
/*                                                                     */
/***********************************************************************/

#include <stdio.h>
#include "sbrk.h"

const size_t _sbrk_size=0x80;	/* Specifies the minimum unit of	*/
								/* the defined heap area			*/ 
#pragma section heap

static  union  {
		long  dummy ;        /* Dummy for 4-byte boundary          */
		char heap[HEAPSIZE]; /* Declaration of the area managed   */
                             /*                           by sbrk */
 }heap_area ;

#pragma section

static  char  *brk=(char *)&heap_area;/* End address of area assigned     */

/**************************************************************************/
/*     sbrk:Data write							  						  */
/*	    Return value:Start address of the assigned area (Pass)	  		  */
/*	                 -1				    (Failure)	  					  */
/**************************************************************************/

extern char  *sbrk(size_t size);
char  *sbrk(size_t size)		   /* Assigned area size	  */
{
      char  *p;

      if(brk+size>heap_area.heap+HEAPSIZE) /* Empty area size		  */
	   return (char *)-1 ;

      p=brk ;				   /* Area assignment		  */
      brk += size ;			   /* End address update	  */
      return p ;
}
