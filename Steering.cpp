///////////////////////////////////////////////////////////
//  Steering.cpp
//  Implementation of the Class Steering
//  Created on:      2018/10/23 11:59:03
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Steering.h"


Steering::Steering(){
	steering_range = 5;	/*方向の初期状態（まっすぐ）*/
	the_direction = 0;	/*アドレス初期値*/
	the_if = 0;	/*アドレス初期値*/
	timer_id = 0;
}



Steering::Steering(int task_id){

}


Steering::~Steering(){

}


/*
*mode_stateによってタイマを動作させる
*/
void Steering::controlSteering(int mode_state){
	switch (mode_state)
	{

		/*
		*Modeの状態がeRUNになったらsteeringに対し100ms後にタイマイベントを送信
		*タイマIDを格納
		*/
	case eRUN:
		timer_id = the_if->set_timer(100, this);
		break;
	case eSTOP:
		result_timer = the_if->cancel_timer(timer_id);	/*eSTOPになったらタイマをキャンセルする*/
		break;
	default:	/*eSTOP_WAITではタイマが動作したままでいてほしいのでなにもしない*/
		break;
	}
}


int Steering::init(LineJudgement * line_judgment, MechIf * mech_if){
	the_direction = line_judgment;
	the_if = mech_if;
	return 0;
}


VOID Steering::timeout(unsigned long id){

	if (the_direction->judgeLine())	/*ライン上*/
	{
		right_max = 10;	/*右の最大値*/
		left_max = 0;	/*左の最大値*/
		if (steering_range < right_max) {
			the_steer_motor->SetMotor(eMotorB, MOTOR_FORE, 0x1770);	/*右に回転*/
			steering_range++;
		}
		else if (steering_range >= right_max) {
			the_steer_motor->SetMotor(eMotorB, MOTOR_BACK, 0x1770);	/*左に回転*/
			steering_range--;
		}
		else
		{
			/*起こらない*/
		}
	}
	else {	/*ライン外*/
		if (steering_range > left_max) {
			the_steer_motor->SetMotor(eMotorB, MOTOR_BACK, 0x1770);	/*左に回転*/
			steering_range--;
		}
		else if (steering_range <= left_max) {
			the_steer_motor->SetMotor(eMotorB, MOTOR_FORE, 0x1770);	/*右に回転*/
			steering_range++;
		}
		else
		{
			/*起こらない*/
		}
	}
	timer_id = the_if->set_timer(100, this);
}

/*
*Modeクラスから非同期メッセージを受け取る
*mode_stateで操舵の制御をする
*/
void Steering::receive(MESSAGE_TYPE type, void * arg){

	switch (type)
	{

	case MSG_RUN_MODE: {
		Mode_state ms = *(Mode_state*)arg;
		controlSteering(ms);
	}
		break;
	case MSG_TIMEOUT:
		//timeoutをよぶと操舵してしまう
		//controlSteeringを呼ぶと役割がかぶる
		break;
	default:
		break;
	}
}