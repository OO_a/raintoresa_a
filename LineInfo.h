///////////////////////////////////////////////////////////
//  LineInfo.h
//  Implementation of the Class LineInfo
//  Created on:      2018/10/23 11:58:08
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_28271809_4E7C_4ac7_85A6_56EF8061EC1E__INCLUDED_)
#define EA_28271809_4E7C_4ac7_85A6_56EF8061EC1E__INCLUDED_

#include "SensorDriver.h"

class LineInfo
{

public:
	LineInfo();
	~LineInfo();
	int getLineInfo();
	int init(SensorDriver *sensor_driver);

private:
	int line_info;
	int getting_method;
	SensorDriver *the_sensor;

};
#endif // !defined(EA_28271809_4E7C_4ac7_85A6_56EF8061EC1E__INCLUDED_)
