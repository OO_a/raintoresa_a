///////////////////////////////////////////////////////////
//  MotorDriver_Real.h
//  Implementation of the Class MotorDriver_Real
//  Created on:      2018/10/22 16:11:29
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_0AEAC18B_CD93_4ca1_81FD_1A46482A0A00__INCLUDED_)
#define EA_0AEAC18B_CD93_4ca1_81FD_1A46482A0A00__INCLUDED_

#include "MotorDriver.h"

class MotorDriver_Real : public MotorDriver
{

public:
	MotorDriver_Real();
	~MotorDriver_Real();
	void SetMotorA(int action, unsigned short speed);
	void SetMotorB(int action);
	void SetMotor(int port, int action, unsigned short speed);
	void SetMotor();

};
#endif // !defined(EA_0AEAC18B_CD93_4ca1_81FD_1A46482A0A00__INCLUDED_)
