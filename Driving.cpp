///////////////////////////////////////////////////////////
//  Driving.cpp
//  Implementation of the Class Driving
//  Created on:      2018/10/23 11:59:17
//  Original author: p000527170
///////////////////////////////////////////////////////////

#include "Driving.h"

using namespace std;



Driving::Driving(){
	driving_state = eDRIVESTOP;	/*driving_stateの初期状態*/
	the_drive_motor = 0;	/*アドレス初期値*/
}


Driving::~Driving(){

}


void Driving::controlDriving(Mode_state mode_state){	/*Modeクラスのmode_stateから状態を変更*/
	switch (mode_state)
	{
	case eSTOP:
		driving_state = eDRIVESTOP;
		the_drive_motor->SetMotor(eMotorA, MOTOR_STOP, 0x1770);	/*駆動モーターを停止させる、速度適当*/
		break;
	case eRUN:
		driving_state = eDRIVERUN;
		the_drive_motor->SetMotor(eMotorA, MOTOR_FORE, 0x1770);	/*駆動モーターを走行させる*/
		break;
	default:
		break;
	}
}


int Driving::init(MotorDriver * motor_driver){
	the_drive_motor = motor_driver;
	return 0;
}
