/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Task_Real
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\Task_Real.h
*********************************************************************/

#ifndef Task_Real_H
#define Task_Real_H

//## class Task_Real
#include "Task.h"
//## auto_generated
class List;

//## dependency ListIterator
class ListIterator;

//## dependency Message
class Message;

//## dependency TimerMessage
class TimerMessage;

//## package Mechanism::Real

//## class Task_Real
class Task_Real : public Task {
    ////    Constructors and destructors    ////
    
public :

    //## operation Task_Real(SSHORT,sem_t*,priority_t,size_t)
    Task_Real(SSHORT rv_id, sem_t* rv_sem, priority_t rv_pri, size_t rv_size = DEFAULT_STACK_SIZE);
    
    //## operation ~Task_Real()
    virtual ~Task_Real();
    
    ////    Operations    ////

private :

    //## operation receive_message(SSHORT)
    VOID receive_message(SSHORT rv_tid);
    
    //## operation start(SSHORT,SCHAR* [])
    VOID start(SSHORT rv_argc, SCHAR* rv_argv[]);
};

#endif
/*********************************************************************
	File Path	: Mechanism\Real\Task_Real.h
*********************************************************************/
