/*enumまとめヘッダー*/
#if !defined(A_GROUP_ENUM_ORIGINAL__INCLUDED_)
#define A_GROUP_ENUM_ORIGINAL__INCLUDED_

enum Driving_state {
	eDRIVESTOP = 0,
	eDRIVERUN
};

enum Mode_state {
	eSTOP = 0,
	eRUN_WAIT,
	eRUN,
	eSTOP_WAIT
};
#endif // !defined(A_GROUP_ENUM_ORIGINAL__INCLUDED_)