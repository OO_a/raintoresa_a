/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: typedefine
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\typedefine\typedefine.h
*********************************************************************/

#ifndef typedefine_H
#define typedefine_H

//## package Mechanism::Sim::typedefine


//## type _SBYTE
typedef char _SBYTE;

//## type _UBYTE
typedef unsigned char _UBYTE;

//## type _SWORD
typedef short _SWORD;

//## type _UWORD
typedef unsigned short _UWORD;

//## type _SINT
typedef int _SINT;

//## type _UINT
typedef unsigned int _UINT;

//## type _SDWORD
typedef long _SDWORD;

//## type _UDWORD
typedef unsigned long _UDWORD;

#endif
/*********************************************************************
	File Path	: Mechanism\Sim\typedefine\typedefine.h
*********************************************************************/
