///////////////////////////////////////////////////////////
//  MotorDriver.h
//  Implementation of the Interface MotorDriver
//  Created on:      2018/10/23 11:26:49
//  Original author: p000527170
///////////////////////////////////////////////////////////

class MotorDriver
{

public:
	MotorDriver() {

	}

	virtual ~MotorDriver() {

	}

	virtual void SetMotor(int port, int action, unsigned short speed) =0;

};
