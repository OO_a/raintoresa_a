///////////////////////////////////////////////////////////
//  Driving.h
//  Implementation of the Class Driving
//  Created on:      2018/10/23 11:59:17
//  Original author: p000527170
///////////////////////////////////////////////////////////

#if !defined(EA_7C594AF5_931B_4e25_8B14_3F34B9E97216__INCLUDED_)
#define EA_7C594AF5_931B_4e25_8B14_3F34B9E97216__INCLUDED_

#include "Driver.h"
#include "Kata.h"
#include "driver.h"
#include "MotorDriver.h"
#include "Kata.h"

class Driving
{

public:
	Driving();
	~Driving();
	void controlDriving(Mode_state mode_state);
	int init(MotorDriver *motor_driver);
private:
	Driving_state driving_state;
	MotorDriver *the_drive_motor;

};
#endif // !defined(EA_7C594AF5_931B_4e25_8B14_3F34B9E97216__INCLUDED_)

