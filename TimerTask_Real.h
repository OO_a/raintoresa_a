/*********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: TimerTask_Real
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Real\TimerTask_Real.h
*********************************************************************/

#ifndef TimerTask_Real_H
#define TimerTask_Real_H

//## dependency cpparch
#include "cpparch.h"
//## class TimerTask_Real
#include "TimerTask.h"
//## auto_generated
class List;

//## dependency ListIterator
class ListIterator;

//## dependency Message
class Message;

//## dependency TimerMessage
class TimerMessage;

//## package Mechanism::Real

//## class TimerTask_Real
class TimerTask_Real : public TimerTask {
    ////    Constructors and destructors    ////
    
public :

    //## operation TimerTask_Real(SSHORT,SLONG)
    TimerTask_Real(SSHORT rv_id, SLONG rv_time);
    
    //## operation ~TimerTask_Real()
    virtual ~TimerTask_Real();
    
    ////    Operations    ////

private :

    //## operation start_timer()
    VOID start_timer();
};

#endif
/*********************************************************************
	File Path	: Mechanism\Real\TimerTask_Real.h
*********************************************************************/
