/********************************************************************
	Copyright (c) 2008 RICOH CO.,Ltd. All rights reserved.
	
	Rhapsody	: 7.5.2
	Project Name	: Project_LineTracer_2013_10_02
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Task_Sim
//!	Generated Date	: xxxx/xx/xx
	File Path	: Mechanism\Sim\Task_Sim.cpp
*********************************************************************/

#include "std.h"

//## auto_generated
#include "Task_Sim.h"
//## auto_generated
#include "List.h"
//## dependency ListIterator
#include "ListIterator.h"
//## dependency Message
#include "Message.h"
//## dependency TimerMessage
#include "TimerMessage.h"
//## package Mechanism::Sim

//## class Task_Sim
Task_Sim::Task_Sim(SSHORT rv_id, sem_t* rv_sem, priority_t rv_pri, size_t rv_size) : Task (rv_id, rv_sem, rv_pri, rv_size) {
    //#[ operation Task_Sim(SSHORT,sem_t*,priority_t,size_t)
    //#]
}

Task_Sim::~Task_Sim() {
    //#[ operation ~Task_Sim()
    //#]
}

VOID Task_Sim::receive_message(SSHORT rv_tid) {
    //#[ operation receive_message(SSHORT)
    //#]
}

VOID Task_Sim::start(SSHORT rv_argc, SCHAR* rv_argv[]) {
    //#[ operation start(SSHORT,SCHAR* [])
    //#]
}

/*********************************************************************
	File Path	: Mechanism\Sim\Task_Sim.cpp
*********************************************************************/
